#!/usr/bin/env python
import urllib2
import urllib
import json
import pprint

# Put the details of the dataset we're going to create into a dict.
dataset_dict = {
    'name': 'my_test_dataset2',
    'notes': 'A long description of my dataset',
    'owner_org': u'ae194ad5-ac2d-4497-b7d1-a841f9c372d6',
}

ckan_site_url = 'http://52.89.136.166'
ckan_apikey = u'7c9eb15e-19fb-4c61-8e22-f154fa902ca5'
ckan_api = '/api/action/'
ckan_cmd = 'package_create'

# Use the json module to dump the dictionary to a string for posting.
data_string = urllib.quote(json.dumps(dataset_dict))

# We'll use the package_create function to create a new dataset.
request = urllib2.Request(ckan_site_url+ckan_api+ckan_cmd)

# Creating a dataset requires an authorization header.
# Replace *** with your API key, from your user account on the CKAN site
# that you're creating the dataset on.
request.add_header('Authorization', ckan_apikey)

# Make the HTTP request.
response = urllib2.urlopen(request, data_string)
assert response.code == 200

# Use the json module to load CKAN's response into a dictionary.
response_dict = json.loads(response.read())
assert response_dict['success'] is True

# package_create returns the created package as its result.
created_package = response_dict['result']
pprint.pprint(created_package)
