#!/usr/bin/env python
import urllib2
import urllib
import json
import pprint
from lxml import etree
# from lxml.etree import Element
from datetime import datetime
import os, sys
# from copy import deepcopy
# from nsutils import NS_SKOS, NS_RDF, NS_XML, NS_RDFS, NS_DC, NS_DCTERMS, NS_OPENSKOS, NS_OAIPMH
# from nsutils import NSMAPOAIPMHOPENSKOS, NSMAPCONCEPT
# from nsutils import nsrdf
from urllib2 import HTTPError


class oai2ckan():
    """ Configure OAI-PM data provider
        get xml from ListSets
        xslt to create dataset dict
        create datasets in ckan
    """
    def __init__(self,config):
        self._readConfig(config)
        self._configOAI()
                      
    def _readConfig(self,configFile):
        # the dict where the configuration is written
        self._config = {}
        config = etree.parse(configFile)
        assert isinstance(config,  etree._ElementTree )
        root = config.getroot()
        
        for element in root.iterchildren():
            assert element.text is not None, "No config value for %s" % element.tag
            self._config[element.tag] = element.text

    def _configOAI(self):
        # the param dict for the http request
        #TODO: filter the records for the DANS OAI provider on dc:type = 'Dataset'
        self.param = {}
        self.changeBaseUrl(self._config['base-url'] )
        self.changeSet(self._config['set'])
    
    def _configCKAN(self):
        self.configCKAN = { 'ckan_site_url' : 'http://52.89.136.166',
                           'ckan_apikey' : u'7c9eb15e-19fb-4c61-8e22-f154fa902ca5',
                           'ckan_api' : '/api/action/',
                           'ckan_cmd' : 'package_create'
        }
        
    def _createDataset(self):
        # Put the details of the dataset we're going to create into a dict.
        self.dataset_dict = {
            'name': 'my_test_dataset2',#TODO: the name of the set
            'notes': 'A long description of my dataset',
            'owner_org': u'ae194ad5-ac2d-4497-b7d1-a841f9c372d6',
        }
        
    def _prepareRequestCkan(self):        

        # We'll use the package_create function to create a new dataset.
        url = self.configCKAN['ckan_site_url']+self.configCKAN['ckan_api']+self.configCKAN['ckan_cmd']
        request = urllib2.Request(url)
        
        # Creating a dataset requires an authorization header.
        # Replace *** with your API key, from your user account on the CKAN site
        # that you're creating the dataset on.
        request.add_header('Authorization', self.configCKAN['ckan_apikey'])
        return request

    def _requestCkan(self):
        self._prepareRequestCkan()
        # Use the json module to dump the dictionary to a string for posting.
        data_string = urllib.quote(json.dumps(self,self.dataset_dict))
        
# Make the HTTP request.
response = urllib2.urlopen(request, data_string)
assert response.code == 200

# Use the json module to load CKAN's response into a dictionary.
response_dict = json.loads(response.read())
assert response_dict['success'] is True

# package_create returns the created package as its result.
created_package = response_dict['result']
pprint.pprint(created_package)


if __name__ == '__main__':
    try:
        if len(sys.argv) == 1:
            """ Only name of the executable, therefor no valid argument """
            raise BaseException("A valid configuration xml file set is expected as argument.")
        configFile = sys.argv[1]
        assert os.path.exists(configFile), "Configuration file %s does not exist." % configFile
        
        t1 = datetime.now()
        mo = oai2ckan(configFile)
        mo.run()
        t2 = datetime.now()
        print 'Done. Total time: ' + str(t2-t1)
    except Exception, e:
        print 'ERROR RAISED: %s' % str(e)
        
        